# Definition of Hotkeys

## Conventions

### Modifier Definition

| Name | Keys Pressed |
|-|-|
| Ctrl | Left Control |
| Alt | Left Alt |
| Super | Windows Key or Command on Mac |
| Shift | Left Shift |
| Alt-Gr | Right Alt (on a German Keyboard)<br />Might just be remapped to <kbd>Ctrl</kbd> + <kbd>Alt</kbd> <br /> Might also be <kbd>Meta</kbd> |
| Meh |  <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>󰘶 Shift</kbd> |
| Hyper | <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>Super</kbd> + <kbd>Shift</kbd> |

### Movement
Where Ever possible we employ VIM like Motions

| Direction | Key |
|-|-|
| Left | <kbd>H</kbd> |
| Down | <kbd>J</kbd> |
| Up   | <kbd>K</kbd> |
| Right| <kbd>L</kbd> |

### Movement Combinations
Here we define, where Modifier Movement Options Go to

| Direction | <kbd>Ctrl</kbd> | <kbd>Alt</kbd> | <kbd>Super</kbd> | <kbd>Ctrl</kbd>+<kbd>󰘶 Shift</kbd> | <kbd>Alt</kbd>+<kbd>󰘶 Shift</kbd> | <kbd>Meh</kbd> | <kbd>Hyper</kbd> |
|-|-|-|-|-|-|-|-|
| <kbd>H</kbd> | App - Tab Prev | App - Move Left  | Win Tile Left  | App - Pane Left  | App - Split Left  | Win Focus Left  ||
| <kbd>J</kbd> |                | App - Move Down  | Win Tile Down  | App - Pane Down  | App - Split Down  | Win Focus Down  ||
| <kbd>K</kbd> |                | App - Move Up    | Win Tile Up    | App - Pane Up    | App - Split Up    | Win Focus Up    ||
| <kbd>L</kbd> | App - Tab Next | App - Move Right | Win Tile Right | App - Pane Right | App - Split Right | Win Focus Right ||

## General

| Modifier | Target |
|-|-|
| Ctrl | Application |
| Alt | OS |
| Ctrl + Shift | Application |
| Super | OS |
| Meh | Application |
| Hyper | OS |

## Application Level

| Function | Hotkey | As Text |
|--|--|--|
| New Tab | <kbd>Ctrl</kbd> + <kbd>T</kbd> |
| Next Tab | <kbd>Ctrl</kbd> + <kbd>L</kbd> |
| Previous Tab | <kbd>Ctrl</kbd> + <kbd>H</kbd> |

## OS Level

| Function | Hotkey | As Text |
|--|--|--|
|Tile Left | <kbd>Super</kbd> + <kbd>H</kbd> |
|Tile Right | <kbd>Super</kbd> + <kbd>L</kbd> |
|Maximize | <kbd>Super</kbd> + <kbd>K</kbd> |
|Previous App | <kbd>Super</kbd> + <kbd>J</kbd> |

### Application Shortcuts
These should directly bring the desired Application to the front.
In Windows, we do this with pinned Taskbar Items, I Linux WM's with assigned Workspaces.
On my Keyboard this is remaped to a Seperate Layer Hotkey

| Application | Hotkey | As Text |
|--|--|--|
| Main Browser | <kbd>Super</kbd> + <kbd>1</kbd> |
| Main Terminal | <kbd>Super</kbd> + <kbd>2</kbd> |
| Main Editor | <kbd>Super</kbd> + <kbd>3</kbd> |
| Main Coms | <kbd>Super</kbd> + <kbd>4</kbd> |
| Main Explorer | <kbd>Super</kbd> + <kbd>5</kbd> |
|  | <kbd>Super</kbd> + <kbd>6</kbd> |
|  | <kbd>Super</kbd> + <kbd>7</kbd> |
|  | <kbd>Super</kbd> + <kbd>8</kbd> |
|  | <kbd>Super</kbd> + <kbd>9</kbd> |
|  | <kbd>Super</kbd> + <kbd>0</kbd> |