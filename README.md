# Hotkey Project



## What's this?

The idea is, to have a common set of Hotkeys across Applications, OS's and Tools.
We never want to think about how to do something, no matter where we work. Whether is VS Code, Firefox or Neovim. Moving through Tabs, splitting views and opening Documents should work the same. In the same vein, Tiling Windows should work the same on Windows, Gnome or KDE.

## Scope

This Project should encompas two things.

### Definition

In step one, we define our prefered Hotkeys. This shouldn't care about environment, but be as general as Possible

### Implementation

In Step two we want to provide settings, tools and software that make our Ideas work wherever we are. Wether that's configs for Window Managers, jsons for VS Code or an Autohotkey Script. What ever we need to get the job done.

### Leader Key

While Modifiers are great and can do a LOT, they reach a limit. Certain functions should work with only the left Hand, to be able to use the mouse while doing so. We also want to stick to Letter keys that are at the Homerow or at most one key off. With those limitations in mind, we can run out of Options.
One Concept from the VIM World that solves this entirely is the Leader key.

We define a Global <kbd>Leader</kbd> Key and any Keypress after that will be intercepted and be remapped to a function. The technical implementation outside of VIM can be a challenge though.
The easiest option BY FAR, is to get a QMK Programable Keyboard. QMK has Leader Functionality baked in and can just remap that to any other Keycombination.

QMK Indenpendent Options for this will be explored later, but not for now. With that in mind, we'll start with the most common functionality and see when we run out of options. At that point, Leader options might be explored for more specific functionality.

## Caveats

I personaly use a kyria Split Ergonomic Keyboard with QMK Firmware. I have a single Button to press "Ctrl + Alt + Shift", or "Meh". This keyboard also has 5 (or more) Thumbkeys for layers and Modifiers. My Setup will make use of those things, as they are easy to reach. This might not translate well to a "normal" keyboard.
At some point, i'd idealy want to have two things: 
- Some option to remap regular keyboards into being more usable (Mapping CapsLock to Meh etc.)
- A way to automatically turn our Definition into config files
Especially the second point would make customizing Hotkeys in the future MUCH easier.